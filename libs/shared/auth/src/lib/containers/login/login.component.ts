import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AuthPartialState, AuthState } from './../../+state/auth.reducer';
import { AuthActionTypes } from '../../+state/auth.actions';
import * as authActions from '../../+state/auth.actions';

@Component({
  selector: 'demo-workspace-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  constructor(private store: Store<AuthPartialState>) {}

  ngOnInit() {}

  login() {
    this.store.dispatch(new authActions.LoginAction());
  }
}
