import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'demo-workspace-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent {
  @Output() login = new EventEmitter<any>();
}
