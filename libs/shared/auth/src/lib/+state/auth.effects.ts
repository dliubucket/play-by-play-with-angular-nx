import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/angular';

import { AuthPartialState, AuthState } from './auth.reducer';
import * as authActions from './auth.actions';
import {
  LoadAuth,
  AuthLoaded,
  AuthLoadError,
  AuthActionTypes
} from './auth.actions';
import { AuthService } from '../services/auth/auth.service';
import { map } from 'rxjs/operators';

@Injectable()
export class AuthEffects {
  @Effect() loadData = this.dataPersistence.fetch(
    authActions.AuthActionTypes.Login,
    {
      // AuthActionTypes.LoadAuth, {
      run: (action: authActions.LoginAction, state: AuthPartialState) => {
        // run: (action: LoadAuth, state: AuthPartialState) => {
        // Your custom REST 'load' logic goes here. For now just return an empty list...
        // return new AuthLoaded([]);
        return this.authService.login().pipe(
          map(user => {
            return new authActions.LoginSuccessAction(user);
          })
        );
      },

      onError: (action: authActions.LoginAction, error) => {
        console.error('Error', error);
        return new AuthLoadError(error);
      }
    }
  );

  constructor(
    private authService: AuthService, // private actions$: Actions,
    private dataPersistence: DataPersistence<AuthPartialState>
  ) {}
}
