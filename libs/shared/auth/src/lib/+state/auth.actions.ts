import { Action } from '@ngrx/store';
import { Entity } from './auth.reducer';

export enum AuthActionTypes {
  Login = '[Auth] Login',
  LoginSuccess = '[Auth] LoginSuccess',
  LoginFail = '[Auth] LoginFail',
  LoadAuth = '[Auth] Load Auth',
  AuthLoaded = '[Auth] Auth Loaded',
  AuthLoadError = '[Auth] Auth Load Error'
}

export class LoadAuth implements Action {
  readonly type = AuthActionTypes.LoadAuth;
}

export class AuthLoadError implements Action {
  readonly type = AuthActionTypes.AuthLoadError;
  constructor(public payload: any) {}
}

export class AuthLoaded implements Action {
  readonly type = AuthActionTypes.AuthLoaded;
  constructor(public payload: Entity[]) {}
}

export class LoginAction implements Action {
  readonly type = AuthActionTypes.Login;
}

export class LoginSuccessAction implements Action {
  readonly type = AuthActionTypes.LoginSuccess;
  constructor(public payload: any) {}
}

export class LoginFailAction implements Action {
  readonly type = AuthActionTypes.LoginFail;
  constructor(public payload: any) {}
}

export type AuthAction =
  | LoginAction
  | LoginSuccessAction
  | LoginFailAction
  | LoadAuth
  | AuthLoaded
  | AuthLoadError;

export const fromAuthActions = {
  LoginAction,
  LoginSuccessAction,
  LoginFailAction,
  LoadAuth,
  AuthLoaded,
  AuthLoadError
};
