# DemoWorkspace

This project was generated using [Nx](https://nx.dev).

<p align="center"><img src="https://raw.githubusercontent.com/nrwl/nx/master/nx-logo.png" width="450"></p>

🔎 **Nx is a set of Angular CLI power-ups for modern development.**

## Quick Start & Documentation

[Nx Documentation](https://nx.dev)

[30-minute video showing all Nx features](https://nx.dev/getting-started/what-is-nx)

[Interactive Tutorial](https://nx.dev/tutorial/01-create-application)

## Adding capabilities to your workspace

Nx supports many plugins which add capabilities for developing different types of applications and different tools.

These capabilities include generating applications, libraries, .etc as well as the devtools to test, and build projects as well.

Below are some plugins which you can add to your workspace:

- [Angular](https://angular.io)
  - `ng add @nrwl/angular`
- [React](https://reactjs.org)
  - `ng add @nrwl/react`
- Web (no framework frontends)
  - `ng add @nrwl/web`
- [Nest](https://nestjs.com)
  - `ng add @nrwl/nest`
- [Express](https://expressjs.com)
  - `ng add @nrwl/express`
- [Node](https://nodejs.org)
  - `ng add @nrwl/node`

## Generate an application

Run `ng g @nrwl/angular:app my-app` to generate an application.

> You can use any of the plugins above to generate applications as well.

When using Nx, you can create multiple applications and libraries in the same workspace.

## Generate a library

Run `ng g @nrwl/angular:lib my-lib` to generate a library.

> You can also use any of the plugins above to generate libraries as well.

Libraries are sharable across libraries and applications. They can be imported from `@demo-workspace/mylib`.

## Development server

Run `ng serve my-app` for a dev server. Navigate to http://localhost:4200/. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng g component my-component --project=my-app` to generate a new component.

## Build

Run `ng build my-app` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test my-app` to execute the unit tests via [Jest](https://jestjs.io).

Run `npm run affected:test` to execute the unit tests affected by a change.

## Running end-to-end tests

Run `ng e2e my-app` to execute the end-to-end tests via [Cypress](https://www.cypress.io).

Run `npm run affected:e2e` to execute the end-to-end tests affected by a change.

## Understand your workspace

Run `npm run dep-graph` to see a diagram of the dependencies of your projects.

## Further help

Visit the [Nx Documentation](https://nx.dev) to learn more.

# Setup CLI Commands

## NPM Installs

[Developing Like Google: Monorepos and Automation](https://nx.dev/angular/fundamentals/develop-like-google)

```
npm install -g @angular/cli @nrwl/schematics
```

## Create New Workspace

```
npx create-nx-workspace demo-workspace --style=css --dry-run
```

- choose `empty` to create an empty workspace when prompted.

## Add a new Application

```
ng add @nrwl/angular
ng g @nrwl/angular:application customer-portal --directory --style=css --routing --dry-run

```

## Adding NgRx to the App

```
ng g ngrx app --module=apps/customer-portal/src/app/app.module.ts --only-empty-root --root --facade=false
```

## Generating Additional Apps

```
ng g @nrwl/angular:application admin-portal --directory --style=css --routing --dry-run
```

## Generating a Lib

```
ng g library auth --routing --lazy --parent-module=apps/customer-portal/src/app/app.module.ts --style scss --directory --dry-run
```

## Regenerating a Lib (to a different folder)

```
-- remdir libs\auth\
-- delete 'auth' section from angular.json
-- remove the route path 'auth' from apps\customer-portal\src\app\app.module.ts
ng g library auth --routing --lazy --parent-module=apps/customer-portal/src/app/app.module.ts --style scss --directory=shared --dry-run
```

## Adding NgRx to a Lib

```
ng g ngrx auth --module=libs/shared/auth/src/lib/shared-auth.module.ts --root --facade=false --dry-run
```

Need to maually update the environment file path:
from
import { environment } from '../environments/environment';
to
import { environment } from '@env/environment';

where @env is defined in tsconfig.jsoni:
"@env/_": ["apps/customer-portal/src/environments/_"]

Note: Use Chrome Redux DevTools to see the State Store.

## Generating a Container Component

```
-- https://duncanhunter.gitbook.io/enterprise-angular-applications-with-ngrx-and-nx/3-generating-components-and-nx-lib
ng g c containers/login --project=shared-auth --dry-run

```

Note:

-- path 'shared-auth' is defined in app.module.ts
-- browse to
http://localhost:4201/shared-auth
http://localhost:4201/shared-auth/login

## Creating a Service

```
ng g service services/auth --project=shared-auth --dry-run

-- make a new folder libs\shared\auth\src\lib\services\auth and move the generated auth.service.*.ts files into the folder.

--- update libs\shared\auth\src\lib\shared-auth.module.ts to add AuthService to providers:
libs\shared\auth\src\lib\shared-auth.module.ts
--- Also need to add an import:
import { AuthService } from './services/auth/auth.service';
```

## Create a Login Form Component and Setup NgRx State Management

```
ng g c components/login-form --project=shared-auth --dry-run
```

Note: You can browse to

http://localhost:4201/shared-auth

then click on the "login" button

then use Chrome Redux DevTool to see the NgRx State store content.
